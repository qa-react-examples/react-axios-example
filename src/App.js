import React from 'react';
import axios from 'axios';

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      user: {}
    };

    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    axios.get('/example-user-data.json').then((response) => {
      this.setState({ user: response.data });
    })
  }

  render() {
    return (
      <div className="App">

      </div>
    );
  }
}
